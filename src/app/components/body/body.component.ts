import { Component, OnInit } from '@angular/core';
import { TurnService, Turn } from '../../services/turn.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  turns:Turn[] = [];
  constructor(private __turnService: TurnService) { }

  ngOnInit() {
    this.turns = this.__turnService.getTurns();
    console.log(this.turns);
  }

}
