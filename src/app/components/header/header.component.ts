import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  myDate:any = Date;

  constructor() { }

  ngOnInit(): void {
    this.utcTime();
  }

  utcTime(): void {
    setInterval(()=>{
      this.myDate = Date();
    }, 1000);
  }
}
