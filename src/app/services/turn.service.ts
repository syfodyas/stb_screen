import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class TurnService {
  urlTurnApiRest:string =  'http://192.168.234.31/api/stb/patients/attend/';
  token:string = '';

  private turns:Turn[] = [
    { "turn": "FMFUST", "floor_id": 2, "waiting_room_id": 1, "doctor_office_id": 1, "patient_id": 100000001, "appointment_id": 36549879, "start": "2018-05-03 09:00:00", "end": "2018-05-02 10:30:00", "passed": 0, "stb_id": 1, "description_stb": "STB Sala 1", "ip_stb": "0.0.0.0", "mac_stb": " 01:23:45:67:89:a", "version_stb": "1" },
    { "turn": "3M8MQ1", "floor_id": 3, "waiting_room_id": 4, "doctor_office_id": 8, "patient_id": 100000002, "appointment_id": 93216548, "start": "2018-05-03 10:00:00", "end": "2018-04-17 11:00:00", "passed": 0, "stb_id": 1, "description_stb": "STB Sala 1", "ip_stb": "0.0.0.0", "mac_stb": " 01:23:45:67:89:a", "version_stb": "1" },
    { "turn": "PP47CE", "floor_id": 5, "waiting_room_id": 3, "doctor_office_id": 2, "patient_id": 100000004, "appointment_id": 86213939, "start": "2018-05-03 10:50:00", "end": "2008-04-21 11:00:00", "passed": 0, "stb_id": 1, "description_stb": "STB Sala 1", "ip_stb": "0.0.0.0", "mac_stb": " 01:23:45:67:89:a", "version_stb": "1" },
    { "turn": "JY89GT", "floor_id": 1, "waiting_room_id": 2, "doctor_office_id": 2, "patient_id": 100000004, "appointment_id": 86213939, "start": "2018-05-03 10:50:00", "end": "2008-04-21 11:00:00", "passed": 0, "stb_id": 1, "description_stb": "STB Sala 1", "ip_stb": "0.0.0.0", "mac_stb": " 01:23:45:67:89:a", "version_stb": "1" },
    { "turn": "FR34VG", "floor_id": 1, "waiting_room_id": 1, "doctor_office_id": 4, "patient_id": 100000004, "appointment_id": 86213939, "start": "2018-05-03 10:50:00", "end": "2008-04-21 11:00:00", "passed": 0, "stb_id": 1, "description_stb": "STB Sala 1", "ip_stb": "0.0.0.0", "mac_stb": " 01:23:45:67:89:a", "version_stb": "1" },
    { "turn": "BM42KL", "floor_id": 1, "waiting_room_id": 2, "doctor_office_id": 1, "patient_id": 100000004, "appointment_id": 86213939, "start": "2018-05-03 10:50:00", "end": "2008-04-21 11:00:00", "passed": 0, "stb_id": 1, "description_stb": "STB Sala 1", "ip_stb": "0.0.0.0", "mac_stb": " 01:23:45:67:89:a", "version_stb": "1" },
    { "turn": "TU58OP", "floor_id": 1, "waiting_room_id": 3, "doctor_office_id": 12, "patient_id": 100000004, "appointment_id": 86213939, "start": "2018-05-03 10:50:00", "end": "2008-04-21 11:00:00", "passed": 0, "stb_id": 1, "description_stb": "STB Sala 1", "ip_stb": "0.0.0.0", "mac_stb": " 01:23:45:67:89:a", "version_stb": "1" }
  ];

  constructor(public __http:HttpClient) {
  }

  public getTurns(){
    return this.turns;
  }

  public getTurnsStb(){

  }

  private getHeaders():HttpHeaders{
    let headers = new HttpHeaders({
      'token': this.token
    });

    return headers;
  }
}

export interface Turn{
  appointment_id:string;
  description_stb:string;
​  doctor_office_id:number;
​​  end:string;
​​  floor_id:number;
​​  ip_stb:string;
​​  mac_stb:string;
​​  passed:number;
​​  patient_id:number;
​​  start:string;
​​  stb_id:number;
​​  turn:string;
​​  version_stb:string;
​​  waiting_room_id:number;
}
