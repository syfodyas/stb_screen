import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { BodyComponent } from './components/body/body.component';

import { HttpClientModule } from '@angular/common/http';
import { TurnService } from './services/turn.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TopbarComponent,
    BodyComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    TurnService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
